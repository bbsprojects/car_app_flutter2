class Members {
  final int m_id, m_status, m_country, m_area, m_utype;

  final String m_fname, m_lname, m_gender;
  final String m_account, m_pwd;
  final String m_mobile, m_email;
  final String m_address, m_cdate;
  final String m_comment,m_img;
  String token;

  Members(
      {this.m_id,
      this.m_fname,
      this.m_lname,
      this.m_gender,
      this.m_status,
      this.m_account,
      this.m_pwd,
      this.m_mobile,
      this.m_email,
      this.m_country,
      this.m_area,
      this.m_address,
      this.m_cdate,
      this.m_comment,
      this.m_utype,
        this.m_img,
        this.token
      });

  factory Members.fromMap(Map<String, dynamic> json) => new Members(
        m_id: int.tryParse(json['m_id'].toString().trim()),
        m_status: int.tryParse(json['m_status'].toString().trim()),
        m_country: int.tryParse(json['m_country'].toString().trim()),
        m_area: int.tryParse(json['m_area'].toString().trim()),
        m_utype: int.tryParse(json['m_utype'].toString().trim()),
        m_fname: json['m_fname'] ?? '',
        m_lname: json['m_lname'] ?? '',
        m_gender: json['m_gender'] ?? '',
        m_account: json['m_account'] ?? '',
        m_pwd: json['m_pwd'] ?? '',
        m_mobile: json['m_mobile'] ?? '',
        m_email: json['m_email'] ?? '',
        m_address: json['m_address'] ?? '',
        m_cdate: json['m_cdate'] ?? '',
        m_comment: json['m_mobile'] ?? '',
        m_img : json['m_img'] ?? '',
         token: json['token']??'',
      );

  Map<String, dynamic> toMap() => {
        'm_fname': m_fname,
        'm_lname': m_lname,
        'm_gender': m_gender,
        'm_account': m_account,
        'm_pwd': m_pwd,
        'm_mobile': m_mobile,
        'm_email': m_email,
        'm_address': m_address,
        'm_cdate': m_cdate,
        'm_comment': m_comment,
        'm_id': m_id,
        'm_status': m_status,
        'm_country': m_country,
        'm_area': m_area,
        'm_utype': m_utype,
        'm_img'  :m_img??"",
        'token' :token??"",
      };
}

/*
factory Group.fromMap(Map<String, dynamic> json) => new Group(
        grpno: int.tryParse(json['grpno'].toString().trim()),
        grpname: json['grpname']??'',
        grpicon: json['grpicon']??'',
        grpqty: int.tryParse(json['grpqty'].toString().trim()),
        grpactive: 0,
      );

  Map<String, dynamic> toMap() => {
        'grpno': grpno,
        'grpname': grpname,
        'grpicon': grpicon,
        'grpqty':grpqty,
      };
 */
