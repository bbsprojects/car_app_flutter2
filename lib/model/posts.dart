
class Posts {

  final int post_id,post_status,post_view_count
  ,post_createdBy ;
  final String post_title,post_desc,post_content
  ,post_img,post_cdate,post_comment;
  // additional info
final String post_createdBy_lebel;

  Posts({ this.post_id,
    this.post_status,
    this.post_view_count,
    this.post_createdBy,
    this.post_title,
    this.post_desc,
    this.post_content,
    this.post_img,
    this.post_cdate,
    this.post_comment,
    this.post_createdBy_lebel});

 factory Posts.fromMap(Map <String,dynamic>json)=> new Posts(
     post_id:int.tryParse(json['post_id'].toString().trim()),
     post_status:int.tryParse(json['post_status'].toString().trim()),
     post_view_count:int.tryParse(json['post_view_count'].toString().trim()),
     post_createdBy:int.tryParse(json['post_createdBy'].toString().trim()),

     post_title:json['post_title']?? '',
     post_desc:json['post_desc']?? '',
     post_content:json['post_content']?? '',
     post_img:json['post_img']?? '',
     post_cdate:json['post_cdate']?? '',
     post_comment:json['post_comment']?? '',
     post_createdBy_lebel:json['post_createdBy_lebel']?? '',
 );

 Map<String,dynamic>toMap()=>{
   'post_createdBy':post_createdBy,
   'post_title':post_title,
   'post_desc':post_desc,
   'post_content':post_content,
   'post_img':post_img,
   'post_cdate':post_cdate,
   'post_comment':post_comment,
   // not sure
   'post_createdBy_lebel':post_createdBy_lebel,

 };
}