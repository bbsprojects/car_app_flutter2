
 class Parts {
   final int prts_id, p_id, prts_cat, is_new, is_orginal;

   final String prts_name, prts_desc, prts_comment, tag;

   Parts({this.prts_id, this.p_id, this.prts_cat, this.is_new, this.is_orginal,
     this.prts_name, this.prts_desc, this.prts_comment, this.tag});


   factory Parts.fromMap(Map<String, dynamic> json)=>
       new Parts(

         prts_id: int.tryParse(json['prts_id'].toString().trim()),
         p_id: int.tryParse(json['p_id'].toString().trim()),
         prts_cat: int.tryParse(json['prts_cat'].toString().trim()),
         is_new: int.tryParse(json['is_new'].toString().trim()),
         is_orginal: int.tryParse(json['is_orginal'].toString().trim()),


         prts_name: json['prts_name'] ?? '',
         prts_desc: json['prts_desc'] ?? '',
         prts_comment: json['prts_comment'] ?? '',
         tag: json['tag'] ?? '',
       );

   Map<String,dynamic> toMap()=>
       {

         'prts_id': prts_id,
         'p_id': p_id,
         'prts_cat': prts_cat,
         'is_new': is_new,
         'is_orginal': is_orginal,
         'prts_name': prts_name,
         'prts_desc': prts_desc,
         'prts_comment': prts_comment,
         'tag': tag,

       };
 }