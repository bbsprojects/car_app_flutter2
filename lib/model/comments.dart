
class Comments {
  final int cmnt_by,cmnt_post_id,cmnt_category,cmnt_alert;

  final String cmnt_content,cmnt_date ;
// additional info for comments with strings
  final String cmnt_by_lebel,cmnt_category_lebel;

  Comments({this.cmnt_by,
    this.cmnt_post_id,
    this.cmnt_category,
    this.cmnt_alert,
    this.cmnt_content,
    this.cmnt_date,
    this.cmnt_by_lebel,
    this.cmnt_category_lebel});

  factory Comments.fromMap(Map<String,dynamic>json)=>new Comments(
      cmnt_by:int.tryParse(json['cmnt_by'].toString().trim()),
      cmnt_post_id:int.tryParse(json['cmnt_post_id'].toString().trim()),
      cmnt_category:int.tryParse(json['cmnt_category'].toString().trim()),
      cmnt_alert:int.tryParse(json['cmnt_alert'].toString().trim()),

      cmnt_content:json['cmnt_content']?? '',
      cmnt_date:json['cmnt_date']?? '',
    // if you don;t sent comment category name  , do cmntCategory model
      cmnt_category_lebel:json['cmnt_category_lebel']?? '',
    cmnt_by_lebel:json['cmnt_by_lebel']?? '',
  );

  Map<String,dynamic>toMap()=>{
    'cmnt_by':cmnt_by,
    'cmnt_post_id':cmnt_post_id,
    'cmnt_category':cmnt_category,
    'cmnt_alert':cmnt_alert,
    'cmnt_content':cmnt_content,
    'cmnt_date':cmnt_date,
    'cmnt_category_lebel':cmnt_category_lebel,

  };
}