
class Notifications {
  final String n_desc, n_content,n_cdate;
  // more info notifications lebels
  final String n_cat_lebel;

  final int n_id,n_cat,is_seen;

  Notifications({
    this.n_desc, this.n_content, this.n_cdate, this.n_cat_lebel,
    this.n_id, this.n_cat, this.is_seen
});

  factory Notifications.fromMap(Map<String,dynamic>json)=>new Notifications(
      n_id:int.tryParse(json['n_id'].toString().trim()),
      n_cat:int.tryParse(json['n_cat'].toString().trim()),
      is_seen:int.tryParse(json['is_seen'].toString().trim()),

      n_desc:json['n_desc']??'',
      n_content:json['n_content']?? '',
      n_cdate:json['n_cdate']?? '',
      n_cat_lebel:json['n_cat_lebel']?? '',

  );



}