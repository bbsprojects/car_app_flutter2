
class ProductDetails {

  final String  pd_desc,sound_sys ,confert_sys,extras;

  final int pd_id ,pd_size ,pd_longInMile,
      is_rental, is_used, p_id;


  ProductDetails({this.pd_desc,
    this.sound_sys,
    this.confert_sys,
    this.extras,
    this.pd_id,
    this.pd_size,
    this.pd_longInMile,
    this.is_rental,
    this.is_used,
    this.p_id});

  factory ProductDetails.fromMap(Map<String,dynamic>json)=>new ProductDetails(
    pd_id: int.tryParse(json['pd_id'].toString().trim()),
    pd_size: int.tryParse(json['pd_size'].toString().trim()),
    pd_longInMile: int.tryParse(json['pd_longInMile'].toString().trim()),
    is_rental: int.tryParse(json['is_rental'].toString().trim()),
    is_used: int.tryParse(json['is_used'].toString().trim()),
    p_id: int.tryParse(json['p_id'].toString().trim()),


    pd_desc: json['pd_desc']?? '',
    sound_sys: json['sound_sys']?? '',
    confert_sys: json['confert_sys']?? '',
    extras: json['extras']?? '',
  );

  Map <String,dynamic> toMap()=>{
    'pd_id':pd_id,
    'pd_size':pd_size,
    'pd_longInMile':pd_longInMile,
    'is_rental':is_rental,
    'is_used':is_used,
    'p_id':p_id,

    'pd_desc':pd_desc,
    'sound_sys':sound_sys,
    'confert_sys':confert_sys,
    'extras':extras,


  };
}