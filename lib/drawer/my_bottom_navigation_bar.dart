
import 'package:flashy_tab_bar/flashy_tab_bar.dart';
import 'package:flutter/material.dart';
import 'package:my_flutter_app/fragments/drawer_pages/login_page.dart';
import 'package:my_flutter_app/fragments/home_page.dart';

class myBottomNavigationBar extends StatefulWidget {
  @override
  _myBottomNavigationBarState createState() => _myBottomNavigationBarState();
}

class _myBottomNavigationBarState extends State<myBottomNavigationBar> {
  int _selectedIndex = 0;
  List <String> pages=[LoginPage.route_name,CarPage.route_name];
  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      child: FlashyTabBar(
        animationCurve: Curves.linear,
        selectedIndex: _selectedIndex,
        showElevation: true,
        // use this to remove appBar's elevation
        onItemSelected: (index) =>
            setState(() {
              _selectedIndex = index;
              print(index.toString());
               /*Navigator.pushReplacementNamed(
                    context, pages[index]);*/
            }),
        items: [
          FlashyTabBarItem(
            icon: Icon(Icons.favorite),
            title: Text('مفضلتي   ',style: TextStyle(fontSize: 12),),
            activeColor: Theme.of(context).primaryColor,
            inactiveColor: Colors.grey,
          ),
          FlashyTabBarItem(
              icon: Icon(Icons.event),
              title: Text('عروضي  ',style: TextStyle(fontSize: 12),),
              activeColor: Theme.of(context).primaryColor
          ),
          FlashyTabBarItem(
              icon: Icon(Icons.account_circle),
              title: Text('حسابي  ',style: TextStyle(fontSize: 12),),
              activeColor: Theme.of(context).primaryColor
          ),
          FlashyTabBarItem(
              icon: Icon(Icons.settings),
              title: Text('الاعدادات  ',style: TextStyle(fontSize: 12),),
              activeColor: Theme.of(context).primaryColor
          ),
        ],

      ),

    );



  }
}
