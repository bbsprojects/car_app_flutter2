import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_flutter_app/routes/page_routes.dart';
import 'package:my_flutter_app/widgets/drawer_body_item.dart';
import 'package:my_flutter_app/widgets/drawer_header.dart';

// refrence thirdrocktechkno.com/blog/how-to-implement-navigation-drawer-in-flutter
class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Colors.white,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            createDrawerHeader(context),
            createDrawerBodyItem(
                icon: CupertinoIcons.car,
                text: 'سيارات',
                onTap: () {
                  Navigator.pushNamed(context, PageRoutes.home);
                }),
            createDrawerBodyItem(
                icon: Icons.search,
                text: 'قطع غيار',
                onTap: () {
                  Navigator.pushNamed(context, PageRoutes.parts);
                }),
            Divider(
              color: Colors.grey,
            ),
            createDrawerBodyItem(
                icon: Icons.account_circle,
                text: 'حراج',
                onTap: () {
                  Navigator.pushNamed(context, PageRoutes.hrag);
                }),

            createDrawerBodyItem(
                icon: Icons.notifications_active,
                text: 'شركات التجنيد',
                onTap: () {
                  Navigator.pushNamed(context, PageRoutes.tangeed);
                }),
            Divider(
              color: Colors.grey,
            ),

            createDrawerBodyItem(
                icon: Icons.contact_mail,
                text: 'مستلزمات السيارة',
                onTap: () {
                  Navigator.pushNamed(context, PageRoutes.accessories);
                }),
            createDrawerBodyItem(
                icon: Icons.contact_mail,
                text: 'الفنيين',
                onTap: () {
                  Navigator.pushNamed(context, PageRoutes.technicians);
                }),
            createDrawerBodyItem(
                icon: Icons.settings, text: 'الإعدادات', onTap: () {}),
            Divider(color: Colors.grey),

            // the others list for guides screen

            ListTile(
              title: Text('مساعدة'),
              onTap: () {},
            ),
            ListTile(
              title: Text('سياسة الخصوصية'),
              onTap: () {},
            ),
            ListTile(
              title: Text('شارك التطبيق '),
              onTap: () {},
            ),
            ListTile(
              title: Text(' حول التطبيق'),
              subtitle: Text('الإصدار 1.0.0'),
              onTap: () {},
            ),
          ],
        ),
      ),
    );
  }
}
