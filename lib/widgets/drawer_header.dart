import 'package:flutter/material.dart';
import 'package:my_flutter_app/routes/page_routes.dart';

Widget createDrawerHeader(BuildContext context) {
  return DrawerHeader(
      margin: EdgeInsets.zero,
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.fill,
              image: ExactAssetImage('assets/images/bg_second_board.png'))),
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: 70.0,
            left: 16.0,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Text(
                    "اهلا بك في عالم السيارات ",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 25.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                //profile
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            child: InkWell(
              child: Container(
                width: MediaQuery.of(context).size.width * 0.74,
                color: Colors.white,
                child: Padding(
                  padding:
                      const EdgeInsets.only(right: 20, bottom: 10, top: 10),
                  child: Text(
                    'تسجيل دخول',
                    style: TextStyle(
                      color: Colors.black,
                    ),
                    textAlign: TextAlign.right,
                  ),
                ),
              ),
              onTap: () {
                Navigator.pushNamed(context, PageRoutes.login);
              },
            ),
          ),
        ],
      ));
}
