import 'package:flutter/material.dart';
import 'package:my_flutter_app/fragments/drawer_pages/accessoriespage.dart';
import 'package:my_flutter_app/fragments/drawer_pages/hrag_page.dart';
import 'package:my_flutter_app/fragments/drawer_pages/parts_pages.dart';
import 'package:my_flutter_app/fragments/drawer_pages/products_view.dart';
import 'package:my_flutter_app/fragments/drawer_pages/register_page.dart';
import 'package:my_flutter_app/fragments/drawer_pages/tangeed_page.dart';
import 'package:my_flutter_app/fragments/drawer_pages/technicians_page.dart';
import 'package:my_flutter_app/fragments/home_page2.dart';
import 'package:my_flutter_app/fragments/on_boarding/onboarding.dart';
import 'package:my_flutter_app/utils/my_theme.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'fragments/drawer_pages/settings_page.dart';
import 'fragments/drawer_pages/login_page.dart';
import 'fragments/home_page.dart';
import 'fragments/drawer_pages/notifications_page.dart';
import 'fragments/drawer_pages/profile_page.dart';
import 'routes/page_routes.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

bool seen;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  seen = prefs.getBool('seen');

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      localizationsDelegates: [
        GlobalCupertinoLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        Locale("fa", "IR"),
      ],
      locale: Locale("fa", "IR"),

      debugShowCheckedModeBanner: false,
      title: 'Car App',
      theme: myThemeData(),
      initialRoute: !seen || seen == null ? "first" : "/",
      routes: {
        '/': (context) => HomePage(),
        "first": (context) => OnBoarding(),
        PageRoutes.home: (context) => CarPage(),
        PageRoutes.homepage: (context) => HomePage(),
        PageRoutes.setting: (context) => SettingPage(),
        PageRoutes.login: (context) => LoginPage(),
        PageRoutes.profile: (context) => ProfilePage(),
        PageRoutes.notification: (context) => NotificationPage(),
        PageRoutes.register: (context) => Register(),
        PageRoutes.parts: (context) => Parts_Page(),
        PageRoutes.hrag: (context) => HragPage(0),
        PageRoutes.technicians: (context) => TechniciansPage(),
        PageRoutes.accessories: (context) => AccessoriesPage(),
        PageRoutes.tangeed: (context) => TangeedPage(),
        PageRoutes.carpage: (context) => ProductsView(),
      },
      //initialRoute: PageRoutes.on_boarding,
    );
  }
}

/*
void main() async

{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool seen = prefs.getBool('seen');
  Widget _screen;
  if( seen != null || seen ==false){
    _screen=OnBoarding();
  }
  else{
    //Goto home
    _screen = CarPage();
  }
  runApp(MyApp(_screen));
}

class MyApp extends StatelessWidget {
  final Widget _screen;
  MyApp(this._screen);
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: myThemeData,
        home: this._screen,
*/
