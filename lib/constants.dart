import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFFfe5b3d);
const kPrimaryLightColor = Color(0xFFFFECDF);
const kPrimaryGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFFfe7747), Color(0xFFfe5b3d)],
);
const kSecondaryColor = Color(0xFFeeeff1);
const kTextColor = Color(0xFF010103);

const kAnimationDuration = Duration(milliseconds: 200);

final headingStyle = TextStyle(
  fontSize: 28,
  fontWeight: FontWeight.bold,
  color: Colors.black,
  height: 1.5,
);
final priceProductStyle = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.normal,
  color: Colors.black,
);
final nameProductStyle = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.bold,
  color: Colors.black,
);

// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
const String kEmailNullError = "الرجاء ادخال بريدك الإلكتروني";
const String kInvalidEmailError = "الرجاء ادخال بريد الكتروني صحيح";
const String kPassNullError = "الرجاء ادخال كلمة السر";
const String kShortPassError = "كلمة السر قصيرة ";
const String kMatchPassError = "كلمة السر لاتتطابق";
const String kNamelNullError = "الرجاء ادخال اسمك";
const String kInvalidNameError = "الرجاء ادخال اسمك الثلاثي";
const String kPhoneNumberNullError = "الرجاء ادخال رقم موبايلك";
const String kInvalidPhoneNumberError = "الرجاء ادخال رقم موبايل صحيحح";
const String kAddressNullError = "الرجاء ادخال عنوانك";

// product details
const String CarDetails = 'تفاصيل السيارة';
const String CarMoreDetails = 'تفاصيل أخرى';
const String first_dt = 'لماركة';
const String second_dt = 'النوع';
const String third_dt = 'الموديل';
const String forth_dt = 'الحالة';
const String fifth_dt = 'اللون';
const String sixth_dt = 'المواصفات';
const String sev_dt = 'الممشى';

// product more details
const String eigh_dt = 'نظام الصوت';
const String nin_dt = 'نظام الراحة ';
const String tn_dt = 'مميزات إضافية';
const String elv_dt = 'السماح بالأيجار';

const String twv_dt = 'لماركة';
const String thrtn_dt = 'لماركة';

final otpInputDecoration = InputDecoration(
  contentPadding: EdgeInsets.symmetric(vertical: 15),
  border: outlineInputBorder(),
  focusedBorder: outlineInputBorder(),
  enabledBorder: outlineInputBorder(),
);

OutlineInputBorder outlineInputBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(15),
    borderSide: BorderSide(color: kTextColor),
  );
}

InputDecoration textInputDecoration = InputDecoration(
  enabledBorder:
      UnderlineInputBorder(borderSide: new BorderSide(color: Colors.grey)),
  focusedBorder:
      UnderlineInputBorder(borderSide: new BorderSide(color: kPrimaryColor)),
  errorBorder:
      UnderlineInputBorder(borderSide: new BorderSide(color: Colors.red)),
  focusedErrorBorder:
      UnderlineInputBorder(borderSide: new BorderSide(color: Colors.red)),
);
