import 'package:flutter/material.dart';
import 'package:my_flutter_app/drawer/my_drawer.dart';

class SettingPage extends StatefulWidget {
  static const String route_name = '/settingsPage';
  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('لإعدادات'),
      ),
      drawer: MyDrawer(),
      body: Center(
        child: Text('هذخ شاشة الإعدادات'),
      ),
    );
  }
}
