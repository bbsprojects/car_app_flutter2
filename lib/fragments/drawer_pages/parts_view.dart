import 'package:flutter/material.dart';

class PartsView extends StatefulWidget {
  @override
  _PartsViewState createState() => _PartsViewState();
}

class _PartsViewState extends State<PartsView> {

  TextStyle _hashTagStyle =(TextStyle(color: Colors.deepOrange));

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.all(8),
      itemBuilder: (context,position)
      {
        return Padding(
          padding: const EdgeInsets.only(bottom:8.0),
          child: Card(
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _cardHeader(),
                _cardDesc(),
                _cardHashtags(),
                _drawBody(),
                _cardFooter(),
              ],
            ) ,
          ),
        );
      },
      itemCount: 20,
    );
  }

  Widget _cardHeader() {
    bool _is_selected=false;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: CircleAvatar(backgroundImage:
              ExactAssetImage('assets/images/logo_hyundai.png'),
                radius: 30,
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text('صدام أمامي كروم',
                      style: TextStyle(
                        color: Colors.grey.shade500,
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(width: 8,),
                    Text('وكالة ',style: TextStyle(color: Colors.blueAccent),
                    ),
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  children: <Widget>[
                    Text('اكسنت 2014',style: TextStyle(color: Colors.grey),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Text('السعر : 19000',style: TextStyle(color: Colors.blueAccent),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Transform.translate(
              child: IconButton(icon: Icon(Icons.favorite),
                onPressed: (){
                setState(() {
                  _is_selected=true;
                });
                },
                //color:_is_selected ?true? Colors.grey.shade500,
                color: Colors.grey.shade500,
              ),
              offset: Offset(-5,0),
            ),
          ],
        ),
      ],
    );


  }

  Widget _cardDesc() {
    return Padding(
      padding: const EdgeInsets.only(bottom:8.0,left: 16 ,right: 16),
      child: Transform.translate(
        child: Text('مواصفات عن قطعة الغيار وما هي السيارات المتماشية معها ااااااااااااااااااااااااااااااااااااااااااااااااااااااا ',style: TextStyle(
          color: Colors.grey.shade800,
          fontSize: 15,
        ),
        ),
        offset:Offset.fromDirection(0,-10),
      ),
    );
  }

  Widget _cardHashtags() {
    return Container(
      child: Wrap(
        children: <Widget>[
          FlatButton(
            onPressed: (){},
            child: Text('VIN 14 رقم',style: _hashTagStyle,),
          ),

          FlatButton(
            onPressed: (){},
            child: Text('قطع مشابهة',style: _hashTagStyle,),
          ),
        ],
      ),
    );

  }

  Widget _drawBody() {
    return SizedBox(
      width: double.infinity,
      height:MediaQuery.of(context).size.height * 0.25 ,
      child: Image(
        image: ExactAssetImage('assets/images/part.png'),
        fit: BoxFit.cover,
      ),
    );

  }

  Widget _cardFooter() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        FlatButton(
          onPressed: (){}, child: Text('شراء',style: _hashTagStyle,),
        ),
        Row(
          children: <Widget>[
            FlatButton(
              onPressed: (){}, child: Text('تقييم',style: _hashTagStyle,),
            ),
            FlatButton(
              onPressed: (){}, child: Text('المزيد',style: _hashTagStyle,),
            ),
          ],
        ),

      ],
    );
  }
}

