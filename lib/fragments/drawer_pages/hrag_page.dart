import 'package:flutter/material.dart';
import 'package:my_flutter_app/constants.dart';
import 'package:my_flutter_app/fragments/Details_pages/hragpart_details.dart';
import 'package:my_flutter_app/model/parts.dart';
import 'package:my_flutter_app/model/products.dart';
import 'package:cache_image/cache_image.dart';

class HragPage extends StatefulWidget {
  static const String route_name = '/hragPage';
  int _selectedtype = 0;
  HragPage(_selectedtype);
  @override
  _HragPageState createState() => _HragPageState(_selectedtype);
}

class _HragPageState extends State<HragPage> {
  int _selectedtype = 0;
  _HragPageState(_selectedtype);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('حراج'),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    setState(() {
                      _selectedtype = 0;
                    });
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: Text(
                      'سيارات',
                      style: TextStyle(
                          color:
                              _selectedtype == 0 ? Colors.white : kTextColor),
                    ),
                    decoration: BoxDecoration(
                        color:
                            _selectedtype == 0 ? kPrimaryColor : Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      _selectedtype = 1;
                    });
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: Text(
                      'قطع غيار',
                      style: TextStyle(
                          color:
                              _selectedtype == 1 ? Colors.white : kTextColor),
                    ),
                    decoration: BoxDecoration(
                        color:
                            _selectedtype == 1 ? kPrimaryColor : Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                  ),
                ),
              ],
            ),
            (_selectedtype == 0) ? HargCarsDetails() : HargPratsDetails(),
            // (_selectedtype == 0) ? HargPratsDetails() : HargPratsDetails(),
          ],
        ),
      ),
    );
  }
}

class HargCarsDetails extends StatefulWidget {
  @override
  _HargCarsDetailsState createState() => _HargCarsDetailsState();
}

class _HargCarsDetailsState extends State<HargCarsDetails> {
  TextStyle _Author_name =
      TextStyle(fontWeight: FontWeight.bold, color: kTextColor);
  TextStyle _Author_desc =
      TextStyle(color: Colors.blue, fontWeight: FontWeight.w600, fontSize: 14);
  List<Products> lstProducts = [
    Products(p_name: ""),
    Products(p_name: ""),
    Products(p_name: ""),
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Expanded(
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: lstProducts.length,
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: ExactAssetImage(
                                      'assets/images/bg_offer.png'),
                                  fit: BoxFit.cover,
                                ),
                                shape: BoxShape.circle,
                              ),
                              height: 45,
                              width: 45,
                              margin: EdgeInsets.only(left: 8.0, right: 5),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'بي بي سوفت',
                                  style: _Author_name,
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      '15 Min .',
                                      style: TextStyle(
                                          color: Colors.grey.shade500,
                                          fontSize: 12),
                                    ),
                                    GestureDetector(
                                      child: Text(
                                        'هوانداي',
                                        style: _Author_desc,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            GestureDetector(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.place,
                                    color: Colors.black54,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(0),
                                    child: Text(
                                      'الرياض',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.grey[600]),
                                    ),
                                  )
                                ],
                              ),
                              onTap: () {},
                            ),
                          ],
                        ),
                      ],
                    ),
                    InkWell(
                      onTap: () {},
                      child: Container(
                        margin: const EdgeInsets.only(top: 10),
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Container(
                                      width: 100,
                                      height: 100,
                                      child: ClipRRect(
                                          child: FadeInImage(
                                            width: 50,
                                            height: 50,
                                            fit: BoxFit.cover,
                                            placeholder: AssetImage(
                                                'assets/images/bg_second_board.png'),
                                            image: AssetImage(
                                                'assets/images/bg_second_board.png'),
                                          ),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10))),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(right: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            "سيارة اكسنت",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text("تفاصيل السيارة",
                                              textAlign: TextAlign.start),
                                          Row(
                                            children: [
                                              Text('2000k'),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Text('.'),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Text('Petrol'),
                                              Text('.'),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Text('7pic'),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                IconButton(
                                    icon: Icon(
                                      Icons.favorite_border,
                                      color: Colors.grey[600],
                                    ),
                                    onPressed: () {})
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: double.infinity,
                              height: 50,
                              decoration: BoxDecoration(
                                  color: Colors.grey[100],
                                  borderRadius: BorderRadius.circular(10)),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(right: 10),
                                    child: Text(
                                      '1000\$',
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                          color: kTextColor),
                                    ),
                                  ),
                                  IconButton(
                                    icon: Icon(
                                      Icons.comment,
                                      color: Colors.grey[600],
                                    ),
                                    onPressed: () {},
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              );
            }),
      ),
    );
  }
}

class HargPratsDetails extends StatefulWidget {
  @override
  _HargPratsDetailsState createState() => _HargPratsDetailsState();
}

class _HargPratsDetailsState extends State<HargPratsDetails> {
  TextStyle _Author_name =
      TextStyle(fontWeight: FontWeight.bold, color: kTextColor);
  TextStyle _Author_desc =
      TextStyle(color: Colors.blue, fontWeight: FontWeight.w600, fontSize: 14);
  List<Parts> lstParts = [
    Parts(prts_name: "مكينة سيارة تويوتا", prts_desc: "تتتتت"),
    Parts(prts_name: "اطار كبير", prts_desc: "بالبا تالتال تىالتا"),
    Parts(prts_name: "فلتر للمكيف سيارة تويوتا", prts_desc: "تات تلا اال"),
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Expanded(
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: lstParts.length,
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: ExactAssetImage(
                                      'assets/images/bg_offer.png'),
                                  fit: BoxFit.cover,
                                ),
                                shape: BoxShape.circle,
                              ),
                              height: 45,
                              width: 45,
                              margin: EdgeInsets.only(left: 8.0, right: 5),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'بي بي سوفت',
                                  style: _Author_name,
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      '15 Min .',
                                      style: TextStyle(
                                          color: Colors.grey.shade500,
                                          fontSize: 12),
                                    ),
                                    GestureDetector(
                                      child: Text(
                                        'اصلي',
                                        style: _Author_desc,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            GestureDetector(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.place,
                                    color: Colors.black54,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(0),
                                    child: Text(
                                      'الرياض',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.grey[600]),
                                    ),
                                  )
                                ],
                              ),
                              onTap: () {},
                            ),
                          ],
                        ),
                      ],
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HragPartDetails(
                                      part: lstParts[index],
                                    )));
                      },
                      child: Container(
                        margin: const EdgeInsets.only(top: 10),
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Container(
                                      width: 100,
                                      height: 100,
                                      child: ClipRRect(
                                          child: FadeInImage(
                                            width: 50,
                                            height: 50,
                                            fit: BoxFit.cover,
                                            placeholder: AssetImage(
                                                'assets/images/bg_second_board.png'),
                                            image: AssetImage(
                                                'assets/images/bg_second_board.png'),
                                          ),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10))),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(right: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            lstParts[index].prts_name,
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(lstParts[index].prts_desc,
                                              textAlign: TextAlign.start),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                IconButton(
                                    icon: Icon(
                                      Icons.favorite_border,
                                      color: Colors.grey[600],
                                    ),
                                    onPressed: () {})
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: double.infinity,
                              height: 50,
                              decoration: BoxDecoration(
                                  color: Colors.grey[100],
                                  borderRadius: BorderRadius.circular(10)),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(right: 10),
                                    child: Text(
                                      '1000\$',
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                          color: kTextColor),
                                    ),
                                  ),
                                  IconButton(
                                    icon: Icon(
                                      Icons.comment,
                                      color: Colors.grey[600],
                                    ),
                                    onPressed: () {},
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              );
            }),
      ),
    );
  }
}
