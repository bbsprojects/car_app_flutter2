import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_flutter_app/api/member_api.dart';
import 'package:my_flutter_app/components/default_button.dart';
import 'package:my_flutter_app/components/form_error.dart';
import 'package:my_flutter_app/constants.dart';
import 'package:my_flutter_app/model/members.dart';

class Register extends StatefulWidget {
  static const String route_name = '/register';
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  String dropdownValueCity = 'الرياض';
  String dropdownValueArea = 'شارع حدة';
  List<String> lstcty = <String>['الرياض', 'جدة', 'ابها', 'مكه'];
  List<String> lstarea = <String>[
    'شارع حدة',
    'كريتر',
    'المعلا',
    'الشيخ',
    'الشعب',
    'البريقة',
    'التواهي',
    'القلوعه'
  ];
  final _formKey = GlobalKey<FormState>();

  String name;
  String phone;
  String email;
  String password;
  String conform_password;

  bool remember = false;
  final List<String> errors = [];

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('تسجيل حساب'),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 20), // 4%
                Text("تسجيل حساب", style: headingStyle),
                Text(
                  "الرجاء تسجيل بياناتك",
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 20),
                SignUpForm(),
                SizedBox(height: 20),
                Text(
                  'من خلال تسجيل الدخول انت موافق على شروط فريقنا',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.caption,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget SignUpForm() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildNameFormField(),
          SizedBox(height: 20),
          buildPhoneFormField(),
          SizedBox(height: 20),
          buildCityDropdown(),
          SizedBox(height: 20),
          buildAreaDropdown(),
          SizedBox(height: 20),
          buildEmailFormField(),
          SizedBox(height: 20),
          buildPasswordFormField(),
          SizedBox(height: 20),
          buildConformPassFormField(),
          FormError(errors: errors),
          SizedBox(height: 30),
          DefaultButton(
            text: "تسجيل حساب",
            press: () {
              if (_formKey.currentState.validate()) {
                MemberApi memberApi = MemberApi();
                Members mem = Members(
                    m_lname: "",
                    m_fname: "",
                    m_gender: "",
                    m_status: 1,
                    m_account: "",
                    m_pwd: "123",
                    m_mobile: "",
                    m_email: "",
                    m_country: 1,
                    m_area: 1,
                    m_address: "",
                    m_utype: 1,
                    m_cdate: "",
                    m_comment: "");
                memberApi.sendRegestrationInfo(mem);
                _formKey.currentState.save();
                // if all are valid then go to success screen
                //  Navigator.pushNamed(context, CompleteProfileScreen.routeName);
              }
            },
          ),
        ],
      ),
    );
  }

  TextFormField buildConformPassFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => conform_password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.isNotEmpty && password == conform_password) {
          removeError(error: kMatchPassError);
        }
        conform_password = value;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if ((password != value)) {
          addError(error: kMatchPassError);
          return "";
        }
        return null;
      },
      decoration: textInputDecoration.copyWith(
        //  labelText: "تأكيد كلمة السر",
        hintText: "ادخل كلمة السر مرة اخرى",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        // floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.lock),
      ),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
        password = value;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if (value.length < 8) {
          addError(error: kShortPassError);
          return "";
        }
        return null;
      },
      decoration: textInputDecoration.copyWith(
        // labelText: "كلمة السر",
        hintText: "ادخل كلمة السر",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        // floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.lock_outline),
      ),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      onSaved: (newValue) => email = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kEmailNullError);
        } else if (emailValidatorRegExp.hasMatch(value)) {
          removeError(error: kInvalidEmailError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kEmailNullError);
          return "";
        } else if (!emailValidatorRegExp.hasMatch(value)) {
          addError(error: kInvalidEmailError);
          return "";
        }
        return null;
      },
      decoration: textInputDecoration.copyWith(
        // labelText: "البريد الإلكتروني",
        hintText: "ادخل البريد الإلكتروني",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        //floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.email),
      ),
    );
  }

  TextFormField buildPhoneFormField() {
    return TextFormField(
      keyboardType: TextInputType.phone,
      onSaved: (newValue) => phone = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        } else if (value.length > 8) {
          removeError(error: kInvalidPhoneNumberError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPhoneNumberNullError);
          return "";
        } else if (value.length < 9) {
          addError(error: kInvalidPhoneNumberError);
          return "";
        }
        return null;
      },
      decoration: textInputDecoration.copyWith(
        // labelText: "رقم الموبايل",
        hintText: "ادخل رقم الموبايل",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        //  floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.phone),
      ),
    );
  }

  TextFormField buildNameFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => name = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        } else if (value.length > 8) {
          removeError(error: kInvalidPhoneNumberError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kNamelNullError);
          return "";
        } else if (value.length < 9) {
          addError(error: kInvalidNameError);
          return "";
        }
        return null;
      },
      decoration: textInputDecoration.copyWith(
        //labelText: "اسمك",
        hintText: "ادخل اسمك الثلاثي",

        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        //  floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.person),
      ),
    );
  }

  DropdownButton buildCityDropdown() {
    return DropdownButton<String>(
      isExpanded: true,
      value: dropdownValueCity,
      icon: Icon(Icons.home),
      iconSize: 24,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (String newValue) {
        setState(() {
          dropdownValueCity = newValue;
        });
      },
      items: lstcty.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value,
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

  DropdownButton buildAreaDropdown() {
    return DropdownButton<String>(
      isExpanded: true,
      value: dropdownValueArea,
      icon: Icon(Icons.place),
      iconSize: 24,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (String newValue) {
        setState(() {
          dropdownValueArea = newValue;
        });
      },
      items: lstarea.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value,
              ),
            ),
          ),
        );
      }).toList(),
    );
  }
}
