import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_flutter_app/components/default_button.dart';
import 'package:my_flutter_app/constants.dart';
import 'package:my_flutter_app/drawer/my_drawer.dart';
import 'package:my_flutter_app/fragments/newhrag.dart';
import 'package:my_flutter_app/fragments/pop_down_pages/about.dart';
import 'package:my_flutter_app/fragments/pop_down_pages/help.dart';
import 'package:my_flutter_app/fragments/recommendedcars.dart';
import 'package:my_flutter_app/routes/page_routes.dart';

class HomePage extends StatefulWidget {
  static const String route_name = '/homePage2';

  @override
  _HomePageState createState() => _HomePageState();
}

enum PopOutMenu { HELP, ABOUT, CONTACT, SETTINGS }

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  int _selectedIndex;
  List<IconData> listicn = [
    FontAwesomeIcons.car,
    FontAwesomeIcons.productHunt,
    FontAwesomeIcons.warehouse,
    FontAwesomeIcons.fontAwesomeFlag,
    FontAwesomeIcons.carCrash,
    Icons.local_offer,
  ];
  List<String> listCatgoryname = [
    "سيارات",
    "قطع غيار",
    "حراج",
    "مستلزمات سيارات",
    "فنيين",
    "عروض",
  ];

  Widget _buildIcon(int index) {
    return GestureDetector(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 10.0),
              padding: EdgeInsets.only(bottom: 10.0),
              decoration: _selectedIndex == index
                  ? BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: kPrimaryColor,
                    )
                  : BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.white,
                    ),
              child: Container(
                padding: const EdgeInsets.all(10),
                width: 100,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.0),
                      topRight: Radius.circular(10.0)),
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 60,
                      width: 60,
                      decoration: BoxDecoration(
                        color: _selectedIndex == index
                            ? kSecondaryColor
                            : kSecondaryColor,
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      child: Icon(
                        listicn[index],
                        size: 25,
                        color: _selectedIndex == index
                            ? kPrimaryColor
                            : Colors.grey,
                      ),
                    ),
                    Text(
                      '${listCatgoryname[index]}',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: _selectedIndex == index
                              ? kPrimaryColor
                              : kTextColor),
                    )
                  ],
                ),
              ),
            ),
            _selectedIndex == index
                ? Positioned(
                    bottom: -1,
                    child: Icon(
                      FontAwesomeIcons.chevronDown,
                      color: kPrimaryColor,
                      size: 30,
                    ),
                  )
                : Container(),
          ],
        ),
      ),
      onTap: () {
        setState(() {
          _selectedIndex = index;
        });
        print('_selectedIndex $_selectedIndex');
      },
    );
  }

  Widget _buildHragcard() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Card(
        elevation: 0,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  GestureDetector(
                    child: Text('حراج سيارات'),
                    onTap: () {},
                  ),
                  GestureDetector(child: Text('حراج قطع غيار'), onTap: () {})
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: kPrimaryColor)),
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Text('يمكنك الان بيع اي قطعه غيار او سياراتك الان بحراج'),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: DefaultButton(
                        text: "بيع الان",
                        press: () {},
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCarcard() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Card(
        elevation: 0,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  GestureDetector(
                      child: Text('جميع السيارات'),
                      onTap: () {
                        Navigator.pushNamed(context, PageRoutes.carpage);
                      }),
                  GestureDetector(
                    child: Text('بحث بحسب البراند'),
                    onTap: () {},
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildPartcard() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Card(
        elevation: 0,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  GestureDetector(child: Text('مكائن'), onTap: () {}),
                  GestureDetector(
                    child: Text('اطارات'),
                    onTap: () {},
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCard() {
    if (_selectedIndex != null) {
      if (_selectedIndex == 0) {
        return _buildCarcard();
      } else if (_selectedIndex == 1) {
        return _buildPartcard();
      } else if (_selectedIndex == 2) {
        return _buildHragcard();
      } else
        return Container();
    } else
      return Container();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      backgroundColor: kSecondaryColor,
      appBar: AppBar(
          title: Text(
            'سيارات',
            style: TextStyle(fontSize: 20, color: Colors.white),
          ),
          backgroundColor: kPrimaryColor,
          centerTitle: false,
          actions: <Widget>[
            IconButton(
                icon: Icon(
                  Icons.notifications,
                  color: Colors.white,
                ),
                onPressed: () {}),
            IconButton(
                icon: Icon(
                  Icons.shopping_cart,
                  color: Colors.white,
                ),
                onPressed: () {}),
            //    _popOutMenu(context),
          ],
          leading: IconButton(
            icon: SvgPicture.asset("assets/icons/menu.svg"),
            onPressed: () => _scaffoldKey.currentState.openDrawer(),
          )
          // set the tabs
          ),
      drawer: MyDrawer(),
      body: SafeArea(
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 120, right: 20),
              child: Text('سيارات'),
            ),
            SizedBox(
              height: 20.0,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: listicn
                    .asMap()
                    .entries
                    .map((MapEntry map) => _buildIcon(map.key))
                    .toList(),
              ),
            ),
            _buildCard(),
            SizedBox(
              height: 20,
            ),
            RecommendedCars(),
            SizedBox(
              height: 10,
            ),
            NewHarag(),
          ],
        ),
      ),
    );
  }

  _popOutMenu(BuildContext context) {
    return PopupMenuButton<PopOutMenu>(
      color: Colors.white,
      itemBuilder: (context) {
        return [
          PopupMenuItem<PopOutMenu>(
            value: PopOutMenu.ABOUT,
            child: Text('ABOUT'),
          ),
          PopupMenuItem<PopOutMenu>(
            value: PopOutMenu.HELP,
            child: Text('HELP'),
          ),
          PopupMenuItem<PopOutMenu>(
            value: PopOutMenu.CONTACT,
            child: Text('CONTACT'),
          ),
          PopupMenuItem<PopOutMenu>(
            value: PopOutMenu.SETTINGS,
            child: Text('SETTINGS'),
          ),
        ];
      },
      onSelected: (PopOutMenu menu) {
        switch (menu) {
          case PopOutMenu.ABOUT:
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) {
                return AboutPage();
              }),
            );
            break;
          case PopOutMenu.HELP:
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) {
                return HelpPage();
              }),
            );
            break;
          case PopOutMenu.CONTACT:
            break;
          case PopOutMenu.SETTINGS:
            Navigator.pushReplacementNamed(context, PageRoutes.setting);
            break;
        }
        //TODO ;
      },
      icon: Icon(Icons.more_vert),
    );
  }
}
