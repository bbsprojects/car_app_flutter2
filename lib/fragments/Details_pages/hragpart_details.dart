import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_flutter_app/constants.dart';
import 'package:my_flutter_app/model/parts.dart';

class HragPartDetails extends StatefulWidget {
  Parts part;
  HragPartDetails({this.part});
  @override
  _HragPartDetailsState createState() => _HragPartDetailsState(part: part);
}

class _HragPartDetailsState extends State<HragPartDetails> {
  Parts part;
  _HragPartDetailsState({this.part});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('تفاصيل القطعة'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Column(
            children: [
              Card(),
              Container(
                width: double.infinity,
                height: 200,
                color: Colors.amber[200],
              ),
              Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 20, right: 20, top: 10),
                      child: Column(
                        children: [
                          Text(
                            'فحمات',
                            style: nameProductStyle,
                          ),
                          Text(
                            '1000 rs',
                            style: priceProductStyle,
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        IconButton(
                            icon: Icon(Icons.add_shopping_cart,
                                color: Colors.grey),
                            onPressed: () {}),
                        IconButton(
                            icon:
                                Icon(Icons.favorite_border, color: Colors.grey),
                            onPressed: () {}),
                        IconButton(
                            icon: Icon(Icons.add_comment, color: Colors.grey),
                            onPressed: () {}),
                        IconButton(
                            icon: Icon(
                              FontAwesomeIcons.shareSquare,
                              color: Colors.grey,
                            ),
                            onPressed: () {}),
                      ],
                    )
                  ],
                ),
              ),
              _drawMainDetails(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _drawMainDetails() {
    return Card(
      elevation: 0,
      child: Padding(
        padding: const EdgeInsets.only(
          top: 15.0,
          bottom: 25,
        ),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("البراند"),
                SizedBox(
                  width: 40,
                ),
                Text('toyota'),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("الموديل"),
                SizedBox(
                  width: 40,
                ),
                Text('اكسنت'),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("سنة الصنع"),
                SizedBox(
                  width: 40,
                ),
                Text('2020'),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("نوع الفطعة"),
                SizedBox(
                  width: 40,
                ),
                Text('اصلي'),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("الوصف"),
                SizedBox(
                  width: 40,
                ),
                Text('أبيض'),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
