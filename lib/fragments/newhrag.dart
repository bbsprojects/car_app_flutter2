import 'package:flutter/material.dart';
import 'package:my_flutter_app/constants.dart';
import 'package:my_flutter_app/model/products.dart';

class NewHarag extends StatefulWidget {
  @override
  _NewHaragState createState() => _NewHaragState();
}

class _NewHaragState extends State<NewHarag> {
  List<Products> lstProducts = [
    Products(p_name: ""),
    Products(p_name: ""),
    Products(p_name: ""),
    Products(p_name: ""),
    Products(p_name: ""),
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      color: kSecondaryColor,
      child: Column(
        children: <Widget>[
          Container(
            color: kSecondaryColor,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('جديد حراج'),
                  /* GestureDetector(
                    child: Text(
                      'الكل',
                    ),
                    onTap: () {},
                  ),*/
                ],
              ),
            ),
          ),
          Container(
            height: 200,
            color: kSecondaryColor,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: lstProducts.length + 1,
              itemBuilder: (BuildContext context, int index) {
                if (index < lstProducts.length) {
                  //  Products product = lstProducts[index];
                  return InkWell(
                    onTap: () {},
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      width: 150,
                      //   color: Colors.red,
                      child: Stack(
                        alignment: Alignment.topCenter,
                        children: <Widget>[
                          Positioned(
                            bottom: 20.0,
                            child: Container(
                              width: 150,
                              height: 100,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Padding(
                                padding: const EdgeInsets.only(right: 20),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('اسم السيارة'),
                                    Text('20000RS'),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(20),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(0.0, 0.2),
                                    blurRadius: 6.0)
                              ],
                            ),
                            child: Stack(
                              children: <Widget>[
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: Image(
                                    height: 100,
                                    width: 100,
                                    image: AssetImage(
                                        "assets/images/bg_second_board.png"),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                Positioned(
                                  right: 0,
                                  bottom: 0,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      IconButton(
                                        icon: Icon(
                                          Icons.favorite_border,
                                          color: Colors.white,
                                        ),
                                        onPressed: () {},
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                } else {
                  return InkWell(
                    splashColor: Colors.grey,
                    highlightColor: Colors.grey,
                    hoverColor: Colors.grey,
                    onTap: () {
                      print('*********');
                    },
                    child: Container(
                      width: 150,
                      decoration: BoxDecoration(
                        color: kSecondaryColor,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 60,
                            height: 60,
                            decoration: BoxDecoration(
                              color: kPrimaryColor,
                              borderRadius: BorderRadius.circular(30),
                            ),
                            child: Icon(
                              Icons.chevron_right,
                              size: 40,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text('واكثر')
                        ],
                      ),
                    ),
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
