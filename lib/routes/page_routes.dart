import 'package:my_flutter_app/fragments/drawer_pages/accessoriespage.dart';
import 'package:my_flutter_app/fragments/drawer_pages/hrag_page.dart';
import 'package:my_flutter_app/fragments/drawer_pages/login_page.dart';
import 'package:my_flutter_app/fragments/drawer_pages/notifications_page.dart';
import 'package:my_flutter_app/fragments/drawer_pages/parts_pages.dart';
import 'package:my_flutter_app/fragments/drawer_pages/products_view.dart';
import 'package:my_flutter_app/fragments/drawer_pages/profile_page.dart';
import 'package:my_flutter_app/fragments/drawer_pages/register_page.dart';
import 'package:my_flutter_app/fragments/drawer_pages/settings_page.dart';
import 'package:my_flutter_app/fragments/drawer_pages/tangeed_page.dart';
import 'package:my_flutter_app/fragments/drawer_pages/technicians_page.dart';
import 'package:my_flutter_app/fragments/home_page2.dart';
import 'package:my_flutter_app/fragments/on_boarding/onboarding.dart';
import 'package:my_flutter_app/fragments/home_page.dart';

class PageRoutes {
  static const String home = CarPage.route_name;
  static const String homepage = HomePage.route_name;
  static const String setting = SettingPage.route_name;
  static const String login = LoginPage.route_name;
  static const String register = Register.route_name;
  static const String profile = ProfilePage.route_name;
  static const String notification = NotificationPage.route_name;
  static const String on_boarding = OnBoarding.route_name;
  static const String parts = Parts_Page.route_name;
  static const String hrag = HragPage.route_name;
  static const String technicians = TechniciansPage.route_name;
  static const String accessories = AccessoriesPage.route_name;
  static const String tangeed = TangeedPage.route_name;
  static const String carpage = ProductsView.route_name;
}
