import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:my_flutter_app/model/products.dart';
import 'package:my_flutter_app/utils/api_path.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProductApi {
  SharedPreferences sharedPreferences;
  String token;
  Future<List<Products>> fetchProducts(int limit) async {
    List<Products> products_items = List<Products>();
    //  token =sharedPreferences.get(('token'));
    // token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJCQlNPRlQiLCJuYmYiOjE2MDk0NTkyMDEsImV4cCI6MTYwOTQ1OTIwMX0.fUM6RS15v4om99JYU8lX6KqqZJf0hxj9lPy2zuxWE_s";
    String url_get_products = base_api + get_product_api + limit.toString();

    Map<String, String> headers = {
      'Content-Type': 'application/json ',
      'accept': 'application/json ',
      //  'token' :'bearer '+token,
    };
    try {
      var response = await http.get(url_get_products, headers: headers);
      print('response status: ${response.statusCode}');

      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body);
        print('body ${response.body}');

        // other way
        var products = jsonData;
        for (var item in products) {
          products_items.add(Products.fromMap(item));
          print(('productName:${Products.fromMap(item).p_type}'));
        }
      } else {
        print('error 0 : response is not 200 ');
      }
    } catch (Excaption) {
      print('error -9  ');
    }
    return products_items;
  }

  Future<String> sendProductInfo() async {
    String product_api = base_api + create_product_api;
    //String m_id =sharedPreferences.get('m_id');
    // token =sharedPreferences.getString(('token'));
    token =
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJCQlNPRlQiLCJuYmYiOjE2MDk0NTkyMDEsImV4cCI6MTYwOTQ1OTIwMX0.fUM6RS15v4om99JYU8lX6KqqZJf0hxj9lPy2zuxWE_s";

// add it to the product
    Products product = new Products();

    Map<String, String> headers = {
      'Content-Type': 'application/json ',
      'accept': 'application/json ',
      'token': 'bearer ' + token,
    };
    try {
      var response = await http.post(product_api,
          headers: headers, body: json.encode(product.toMap()));

      String gg = json.encode(product.toMap());
      print('Response status: ${response.statusCode}');
      print('body map:$gg');

      // chech status code
      if (response.statusCode == 200) {
        print('Response body: ${response.body}');
        var jsonData = jsonDecode(response.body);
        print('json Data: ${jsonData}');

        return '';
      }

      return '0';
    } catch (Exception) {
      return '-9';
    }
  }
}
