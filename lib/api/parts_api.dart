import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:my_flutter_app/model/parts.dart';
import 'package:my_flutter_app/utils/api_path.dart';

class PartsApi {
  Future<List<Parts>> fetchAllParts() async {
    List<Parts> parts_items = List<Parts>();
    String uri = base_api + '';
    var response = await http.get(uri);
    print('response status: ,${response.statusCode}');

    if (response.statusCode == 200) {
      var jsonData = jsonDecode(response.body);

      var parts = jsonData['parts'];
      for (var item in parts) {
        parts_items.add(Parts.fromMap(item));
      }
    }
    return parts_items;
  }
}
